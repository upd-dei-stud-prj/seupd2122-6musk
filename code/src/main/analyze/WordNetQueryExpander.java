/*
 * Copyright 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package src.main.analyze;

import org.apache.lucene.analysis.*;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;
import org.apache.lucene.wordnet.SynonymMap;
import org.apache.lucene.wordnet.SynonymTokenFilter;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;

import static src.main.analyze.AnalyzerUtil.consumeTokenStream;
import static src.main.analyze.AnalyzerUtil.loadStopList;

/**
 * @author Marco Mariotto (marco.mari8@gmail.com)
 * @version 1.0
 * @since 1.0
 */
public class WordNetQueryExpander extends Analyzer {

    /**
     * Creates a new instance of the analyzer.
     */

    private int maxSynonyms = 3;
    private CharArraySet fromSet = null;

    public WordNetQueryExpander(int maxSynonyms, CharArraySet fromSet) {
        super();
        this.maxSynonyms = maxSynonyms;
        this.fromSet = fromSet;
    }

    public WordNetQueryExpander(){
        super();
    }

    @Override
    protected TokenStreamComponents createComponents(String fieldName) {
        final Tokenizer source = new StandardTokenizer();
        TokenStream tokens = new LowerCaseFilter(source);
        tokens = new StopFilter(tokens, loadStopList("stoplists/glasgow.txt"));
        SynonymMap map;

        try {
             map = new SynonymMap(new FileInputStream("synonyms/wn_s.pl"));
        } catch (IOException e) {
            throw new IllegalArgumentException("Could not find synonym database!");
        }

        tokens = new SynonymTokenFilter(tokens, map, maxSynonyms);

        if(fromSet != null)
            tokens = new KeepOnlyFilter(tokens, fromSet);

        return new TokenStreamComponents(source, tokens);
    }

    @Override
    protected Reader initReader(String fieldName, Reader reader) {
        return super.initReader(fieldName, reader);
    }

    @Override
    protected TokenStream normalize(String fieldName, TokenStream in) {
        return new LowerCaseFilter(in);
    }

    /**
     * Main method of the class.
     *
     * @param args command line arguments.
     *
     * @throws IOException if something goes wrong while processing the text.
     */
    public static void main(String[] args) throws IOException {

        // text to analyze
        final String text = "I will buy a car next week";
        final CharArraySet set = new CharArraySet(Arrays.asList("auto", "succeeding"), true);

        // use the analyzer to process the text and print diagnostic information about each token
        consumeTokenStream(new WordNetQueryExpander(3, set), text);
    }

    public static class KeepOnlyFilter extends FilteringTokenFilter{

        private final CharArraySet keepWords;
        private final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);
        private final TypeAttribute tokenType = addAttribute(TypeAttribute.class);

        public KeepOnlyFilter(TokenStream in, CharArraySet keepWords) {
            super(in);
            this.keepWords = keepWords;
        }

        @Override
        protected boolean accept() throws IOException {
            if(!tokenType.type().equals("SYNONYM")){
                return true;
            }
            else if(keepWords.contains(termAtt.buffer(), 0, termAtt.length())){
                System.out.println("FOUND SYNONYM while expanding query using synonyms from keep list");
                return true;
            }
            return false;
        }
    }


}
