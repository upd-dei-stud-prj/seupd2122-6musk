/*
This analyzer is used to perform query expansion with the Word2Vec library.
The idea is to train the model with the sourceText field from the context. Since it contains much "noise" (such as
repeated word, "xa0", links, etc..) which does not provide valuable information, it has been preprocessed into
args_only_context_txt using the commented part of this file, plus applying orginalContextAsString.substring(40,
length-40).
args_only_context.txt was used in the main of Word2VecFilter to build the vocabulary and model as requested
by the Word2Vec library of deeplearning4j.
 */

/*
 * Copyright 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package src.main.analyze;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.standard.StandardTokenizer;

import java.io.IOException;
import java.io.Reader;

import static src.main.analyze.AnalyzerUtil.consumeTokenStream;

/**
 * Analyzer used for query expansion
 * #commented for parsing context for vocabulary building
 *
 */
public class Word2VecAnalyzer extends Analyzer {

    /**
     * Creates a new instance of the analyzer.
     */
    public Word2VecAnalyzer() {
        super();
    }

    @Override
    protected TokenStreamComponents createComponents(String fieldName) {
        final Tokenizer source = new StandardTokenizer();
        TokenStream tokenStream = new LowerCaseFilter(source);
        tokenStream = new Word2VecFilter(tokenStream);

        /*Pattern p = Pattern.compile("\\S*(\\w)(?=\\1{2,})\\S*");
        boolean replaceAll = Boolean.TRUE;
        tokenStream = new PatternReplaceFilter(tokenStream, p, "", replaceAll);
        Pattern pxa = Pattern.compile("\\b\\S*xa0\\S*");
        tokenStream = new PatternReplaceFilter(tokenStream, pxa, "", replaceAll);

        TokenStream tokens = new LowerCaseFilter(source);
        tokenStream = new HyphenatedWordsFilter(tokenStream);
        tokenStream = new LengthFilter(tokenStream, 3, 13);*/

       /*if(fieldName.equals(ParsedDocument.FIELDS.PREMISES) || fieldName.equals(ParsedDocument.FIELDS.CONCLUSION)){
            //tokens = new StopFilter(tokens, loadStopList("stoplists/smart.txt"));
            tokens = new StopFilter(tokens, loadStopList("stoplists/glasgow.txt"));
            //tokens = new StopFilter(tokens, loadStopList("stoplists/seupd-2122-6musk-stoplist.txt"));
        }*/
        return new TokenStreamComponents(source, tokenStream);
    }

    @Override
    protected Reader initReader(String fieldName, Reader reader) {
        // return new HTMLStripCharFilter(reader);

        return super.initReader(fieldName, reader);
    }

    @Override
    protected TokenStream normalize(String fieldName, TokenStream in) {
        return new LowerCaseFilter(in);
    }

    /**
     * Main method of the class.
     *
     * @param args command line arguments.
     *
     * @throws IOException if something goes wrong while processing the text.
     */
    public static void main(String[] args) throws IOException {

        // text to analyze
        final String text = "I now live in Rome where I met my wife Alice back in 2010 during a beautiful afternoon. " +
                "Occasionally, I fly to New York to visit the United Nations where I would like to work. The last " +
                "time I was there in March 2019, the flight was very inconvenient, leaving at 4:00 am, and expensive," +
                " over 1,500 dollars.";
        //final String text = "For the first time, in its 800 academic year, University of Padua has a female " +
        //		"Rector.";

        // use the analyzer to process the text and print diagnostic information about each token
        consumeTokenStream(new Word2VecAnalyzer(), text);
    }
}