/*
 *  Copyright 2017-2022 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package src.main.index;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.search.similarities.LMDirichletSimilarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.util.BytesRef;
import src.main.analyze.ToucheAnalyzer;
import src.main.parse.DocumentParser;
import src.main.parse.ParsedDocument;
import src.main.parse.ToucheParser;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;



import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * Indexes documents processing a whole directory tree.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */


public class DirectoryIndexerDocument extends AbstractDirectoryIndexer{

    /**
     * The file where to write the vocabulary
     */
    private final Path vocabularyPath;

    /**
     * Creates a new indexer for the documents.
     *
     * @param analyzer        the {@code Analyzer} to be used.
     * @param similarity      the {@code Similarity} to be used.
     * @param ramBufferSizeMB the size in megabytes of the RAM buffer for indexing documents.
     * @param indexPath       the directory where to store the index.
     * @param docsPath        the directory from which documents have to be read.
     * @param extension       the extension of the files to be indexed.
     * @param charsetName     the name of the charset used for encoding documents.
     * @param expectedDocs    the total number of documents expected to be indexed
     * @param dpCls           the class of the {@code DocumentParser} to be used.
     * @throws NullPointerException     if any of the parameters is {@code null}.
     * @throws IllegalArgumentException if any of the parameters assumes invalid values.
     */
    public DirectoryIndexerDocument(Analyzer analyzer, Similarity similarity, int ramBufferSizeMB, String indexPath, String docsPath, String extension, String charsetName, long expectedDocs, Class<? extends DocumentParser> dpCls, final String vocabularyFile) {
        super(analyzer, similarity, ramBufferSizeMB, indexPath, docsPath, extension, charsetName, expectedDocs, dpCls);

        vocabularyPath = Paths.get(vocabularyFile);
        if (Files.isDirectory(vocabularyPath)) {
            throw new IllegalArgumentException(
                    String.format("%s expected to be a file where to write the vocabulary.", vocabularyPath.toAbsolutePath().toString()));
        }
    }

    /**
     * Indexes the documents.
     *
     * @throws IOException if something goes wrong while indexing.
     */
    public void index() throws IOException {

        System.out.printf("%n#### Start indexing ####%n");

        Files.walkFileTree(docsDir, new SimpleFileVisitor<>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                if (file.getFileName().toString().endsWith(extension)) {

                    DocumentParser dp = DocumentParser.create(dpCls, Files.newBufferedReader(file, cs));

                    bytesCount += Files.size(file);

                    filesCount += 1;

                    Document doc;

                    for (ParsedDocument pd : dp) {

                        doc = new Document();

                        // add the document identifier
                        doc.add(new StringField(ParsedDocument.FIELDS.ID, pd.getIdentifier(), Field.Store.YES));

                        //add the SourceID, which is the "full" document identifier
                        doc.add(new StringField(ParsedDocument.FIELDS.SOURCE_ID, pd.getSourceId(), Field.Store.YES));

                        // add the conclusion
                        doc.add(new TextField(ParsedDocument.FIELDS.CONCLUSION, pd.getConclusion(), Field.Store.YES));

                        //add the premises text
                        doc.add(new TextField(ParsedDocument.FIELDS.TEXT, pd.getText(), Field.Store.NO));

                        //store the sentences in the index //TODO it's here for legacy reasons, could be removed
                        byte[] data = SerializationUtils.serialize(pd.getSentences());
                        doc.add(new StoredField(ParsedDocument.FIELDS.SENTENCES, new BytesRef(data)));

                        //add the stance field
                        doc.add(new StringField(ParsedDocument.FIELDS.STANCE, pd.getStance(), Field.Store.YES));

                        writer.addDocument(doc);

                        docsCount++;

                        // print progress every 10000 indexed documents
                        if (docsCount % 10000 == 0) {
                            System.out.printf("%d document(s) (%d files, %d Mbytes) indexed in %d seconds.%n",
                                    docsCount, filesCount, bytesCount / MBYTE,
                                    (System.currentTimeMillis() - start) / 1000);
                        }

                    }

                }
                return FileVisitResult.CONTINUE;
            }
        });

        writer.commit();

        writer.close();

        if (docsCount != expectedDocs) {
            System.out.printf("Expected to index %d documents; %d indexed instead.%n", expectedDocs, docsCount);
        }

        System.out.printf("%d document(s) (%d files, %d Mbytes) indexed in %d seconds.%n", docsCount, filesCount,
                          bytesCount / MBYTE, (System.currentTimeMillis() - start) / 1000);

        System.out.printf("#### Indexing complete ####%n");
    }

    /**
     * Prints statistics about the vocabulary to a file.
     *
     * @throws IOException if something goes wrong while accessing the index.
     */
    public void printVocabularyStatistics() throws IOException {

        System.out.printf("%n------------- PRINTING VOCABULARY STATISTICS -------------%n");

        // Keep statistics about the vocabulary. Each key is a term; each value is an array where the first element is
        // the total frequency of the term and the second element is the document frequency.
        final Map<String, long[]> stats = new HashMap<>(2000000);

        // create a new vocabulary file, replacing the existing one, if any
        final PrintWriter out = new PrintWriter(Files.newBufferedWriter(vocabularyPath, StandardCharsets.UTF_8, StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE));

        // Open the directory in Lucene
        final Directory dir = FSDirectory.open(indexDir);

        // Get a reader for the index
        final IndexReader index = DirectoryReader.open(dir);

        // total number of documents
        long totDocs = 0;

        // total number of terms
        long totTerms = 0;

        // get the leaf readers, i.e. the readers for each segment of the index
        for(LeafReaderContext lrc : index.leaves()) {

            LeafReader lidx = lrc.reader();

            // Increment the total number of documents
            totDocs += lidx.numDocs();

            // Get the vocabulary of this leaf index.
            Terms voc = lidx.terms(ParsedDocument.FIELDS.TEXT);

            // Get an iterator over the vector of terms in the vocabulary
            TermsEnum termsEnum = voc.iterator();

            // Iterate until there are terms
            for (BytesRef term = termsEnum.next(); term != null; term = termsEnum.next()) {

                // Get the text string of the term
                String termstr = term.utf8ToString();

                // Get the total frequency of the term
                long freq = termsEnum.totalTermFreq();

                // Increment the total number of terms
                totTerms += freq;

                // Get the document frequency (DF) of the term
                int df = termsEnum.docFreq();

                // update the statistics with the new entry
                stats.compute(termstr, (k, v) -> {

                    // if the term is not already in the statistics, create a new pair for its counts
                    if (v == null) {
                        v = new long[2];
                    }

                    // Update the counts
                    v[0] += freq;
                    v[1] += df;

                    // return the updated counts
                    return v;
                });
            }

        }

        // close index and directory
        index.close();
        dir.close();

        System.out.printf("+ Total number of documents %d%n", totDocs);

        System.out.printf("+ Total number of unique terms: %d%n", stats.size());

        System.out.printf("+ Total number of terms: %d%n", totTerms);

        // Print vocabulary statistics to the output file
        System.out.printf("+ Printing vocabulary statistics to %s%n", vocabularyPath.toAbsolutePath().toString());
        out.printf("%s\t%s\t%s%n", "Term", "RawFrequency", "DocumentFrequency");

        stats.forEach( (k, v) -> {
            out.printf("%s\t%d\t%d%n", k, v[0], v[1]);
        });

        // close vocabulary file
        out.flush();
        out.close();

        System.out.printf("----------------------------------------------------------%n");
    }


    /**
     * Main method of the class. Just for testing purposes.
     *
     * @param args command line arguments.
     * @throws Exception if something goes wrong while indexing.
     */
    public static void main(String[] args) throws Exception {

        final int ramBuffer = 2048;
        final String docsPath = "corpus";
        final String indexPath = "experiment/index-document-stop-kstem";
        final String vocabularyFile = "experiment/vocabulary.txt";

        final String extension = "csv";
        final int expectedDocs = 365408;
        final String charsetName = "UTF-8";

        DirectoryIndexerDocument i = new DirectoryIndexerDocument(new ToucheAnalyzer(), new LMDirichletSimilarity(), ramBuffer, indexPath, docsPath, extension,
                                                  charsetName, expectedDocs, ToucheParser.class, vocabularyFile);
        i.index();
        i.printVocabularyStatistics();
    }
}
