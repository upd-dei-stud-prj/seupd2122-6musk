/*
 *  Copyright 2017-2022 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package src.main.index;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.search.similarities.LMDirichletSimilarity;
import org.apache.lucene.search.similarities.Similarity;
import src.main.analyze.ToucheAnalyzer;
import src.main.parse.DocumentParser;
import src.main.parse.ParsedDocument;
import src.main.parse.ParsedSentence;
import src.main.parse.ToucheParser;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Indexes documents processing a whole directory tree.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class DirectoryIndexerSentences extends AbstractDirectoryIndexer{

    /**
     * Creates a new indexer for sentences.
     *
     * @param analyzer        the {@code Analyzer} to be used.
     * @param similarity      the {@code Similarity} to be used.
     * @param ramBufferSizeMB the size in megabytes of the RAM buffer for indexing documents.
     * @param indexPath       the directory where to store the index.
     * @param docsPath        the directory from which documents have to be read.
     * @param extension       the extension of the files to be indexed.
     * @param charsetName     the name of the charset used for encoding documents.
     * @param expectedDocs    the total number of documents expected to be indexed
     * @param dpCls           the class of the {@code DocumentParser} to be used.
     * @throws NullPointerException     if any of the parameters is {@code null}.
     * @throws IllegalArgumentException if any of the parameters assumes invalid values.
     */
    public DirectoryIndexerSentences(Analyzer analyzer, Similarity similarity, int ramBufferSizeMB, String indexPath, String docsPath, String extension, String charsetName, long expectedDocs, Class<? extends DocumentParser> dpCls) {
        super(analyzer, similarity, ramBufferSizeMB, indexPath, docsPath, extension, charsetName, expectedDocs, dpCls);
    }

    /**
     * Indexes the sentences.
     *
     * @throws IOException if something goes wrong while indexing.
     */
    public void index() throws IOException {

        System.out.printf("%n#### Start indexing ####%n");

        Files.walkFileTree(docsDir, new SimpleFileVisitor<>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                if (file.getFileName().toString().endsWith(extension)) {

                    DocumentParser dp = DocumentParser.create(dpCls, Files.newBufferedReader(file, cs));

                    bytesCount += Files.size(file);

                    filesCount += 1;

                    Document sentence;
                    String sentence_type;

                    for (ParsedDocument pd : dp) {

                        ParsedSentence[] sentences = pd.getSentences();
                        for (ParsedSentence parsedSentence : sentences) {
                            sentence = new Document();
                            sentence.add(new TextField(ParsedSentence.FIELDS.SENT_TEXT, parsedSentence.getSent_text(), Field.Store.YES));
                            sentence.add(new StringField(ParsedSentence.FIELDS.SENT_ID, parsedSentence.getSent_id(), Field.Store.YES));
                            sentence.add(new StringField(ParsedDocument.FIELDS.ID, pd.getIdentifier(), Field.Store.YES));

                            // sentence is a premise
                            if (!parsedSentence.getSent_id().matches("\\w+-\\w+__CONC__\\w+"))
                                sentence_type = "PREMISE";
                            //sentence is a conclusion
                            else
                                sentence_type = "CONC";

                            sentence.add(new StringField(ParsedSentence.FIELDS.TYPE, sentence_type, Field.Store.YES));

                            writer.addDocument(sentence);
                            docsCount++;

                            // print progress every 100000 indexed sentences
                            if (docsCount % 100000 == 0) {
                                System.out.printf("%d sentence(s) (%d files, %d Mbytes) indexed in %d seconds.%n",
                                        docsCount, filesCount, bytesCount / MBYTE,
                                        (System.currentTimeMillis() - start) / 1000);
                            }
                        }


                    }

                }
                return FileVisitResult.CONTINUE;
            }
        });

        writer.commit();

        writer.close();

        if (docsCount != expectedDocs) {
            System.out.printf("Expected to index %d sentences; %d indexed instead.%n", expectedDocs, docsCount);
        }

        System.out.printf("%d sentences(s) (%d files, %d Mbytes) indexed in %d seconds.%n", docsCount, filesCount,
                          bytesCount / MBYTE, (System.currentTimeMillis() - start) / 1000);

        System.out.printf("#### Indexing complete ####%n");
    }

    /**
     * Main method of the class. Just for testing purposes.
     *
     * @param args command line arguments.
     * @throws Exception if something goes wrong while indexing.
     */
    public static void main(String[] args) throws Exception {

        final int ramBuffer = 2048;
        final String docsPath = "corpus";
        final String indexPath = "experiment/index-sentence-stop-kstem";

        final String extension = "csv";
        final int expectedDocs = 6118957;
        final String charsetName = "UTF-8";

        DirectoryIndexerSentences i = new DirectoryIndexerSentences(new ToucheAnalyzer(), new LMDirichletSimilarity(), ramBuffer, indexPath, docsPath, extension,
                                                  charsetName, expectedDocs, ToucheParser.class);
        i.index();
    }
}
