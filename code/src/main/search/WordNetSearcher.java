package src.main.search;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.benchmark.quality.QualityQuery;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParserBase;
import org.apache.lucene.search.*;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;
import src.main.analyze.WordNetQueryExpander;
import src.main.parse.ParsedDocument;
import src.main.parse.ParsedSentence;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class WordNetSearcher extends AbstractSearcher{


    /**
     * The index reader for the sentences
     */
    private final IndexReader readerSentences;

    /**
     * The index searcher for the sentences
     */
    private final IndexSearcher searcherSentences;

    /**
     * Creates a new searcher.
     *
     * @param similarity       the {@code Similarity} to be used.
     * @param indexPath        the directory where containing the index to be searched.
     * @param indexSentPath    the directory where containing the index of sentences to be searched.
     * @param topicsFile       the file containing the topics to search for.
     * @param expectedTopics   the total number of topics expected to be searched.
     * @param runID            the identifier of the run to be created.
     * @param runPath          the path where to store the run.
     * @param maxDocsRetrieved the maximum number of documents to be retrieved.
     * @throws NullPointerException     if any of the parameters is {@code null}.
     * @throws IllegalArgumentException if any of the parameters assumes invalid values.
     */
    public WordNetSearcher(Similarity similarity, String indexPath, String indexSentPath, String topicsFile, int expectedTopics, String runID, String runPath, int maxDocsRetrieved) {
        super(similarity, indexPath, topicsFile, expectedTopics, runID, runPath, maxDocsRetrieved);

        final Path indexDir = Paths.get(indexSentPath);
        if (!Files.isReadable(indexDir)) {
            throw new IllegalArgumentException(
                    String.format("Index directory %s cannot be read.", indexDir.toAbsolutePath().toString()));
        }

        if (!Files.isDirectory(indexDir)) {
            throw new IllegalArgumentException(String.format("%s expected to be a directory where to search the index.",
                    indexDir.toAbsolutePath().toString()));
        }

        try {
            readerSentences = DirectoryReader.open(FSDirectory.open(indexDir));
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("Unable to create the index reader for directory %s: %s.",
                    indexDir.toAbsolutePath().toString(), e.getMessage()), e);
        }

        searcherSentences = new IndexSearcher(readerSentences);
        searcherSentences.setSimilarity(similarity);
    }

    @Override
    protected String decideSentencesToReturn(Document doc, String queryTitle) throws ParseException, IOException{
        String result;
        HashMap<String, String> values = new HashMap<>();
        values.put(ParsedDocument.FIELDS.CONCLUSION, doc.get(ParsedDocument.FIELDS.CONCLUSION));
        values.put(ParsedDocument.FIELDS.ID, doc.get(ParsedDocument.FIELDS.ID));

        final Set<String> idField_sentence = new HashSet<>();
        idField_sentence.add(ParsedSentence.FIELDS.ID);
        idField_sentence.add(ParsedSentence.FIELDS.TYPE);
        idField_sentence.add(ParsedSentence.FIELDS.SENT_ID);
        idField_sentence.add(ParsedSentence.FIELDS.SENT_TEXT);

        BooleanQuery.Builder bq = new BooleanQuery.Builder();
        //Expand queryTitle using WordNet analyzer and look for the query on sent_text
        //No specific keep list is set here; maybe TODO?
        QueryParser qp = new QueryParser(ParsedSentence.FIELDS.SENT_TEXT, new WordNetQueryExpander());
        bq.add(qp.parse(QueryParserBase.escape(queryTitle)), BooleanClause.Occur.SHOULD);
        //select only sentences coming from the current document doc
        Query idQuery = new WildcardQuery(new Term(ParsedDocument.FIELDS.ID, values.get(ParsedDocument.FIELDS.ID)));
        bq.add(idQuery, BooleanClause.Occur.MUST);

        Query q = bq.build();
        //search the best two sentences for this document
        TopDocs sentences_retrieved = searcherSentences.search(q, 2);
        ScoreDoc[] scored_sentences = sentences_retrieved.scoreDocs;

        //if less than 2, skip document (for instance there's only a conclusion)
        //TODO a more sophisticated approach would select sentences coming from different documents!
        //however then one needs to check whether the two sentences retrieved are coherent or not
        //here instead they are coherent since they come from the same document, and also they are the best match
        //against our query (among all sentences of the same document)
        if(scored_sentences.length >= 2)
            result = readerSentences.document(scored_sentences[0].doc, idField_sentence).get(ParsedSentence.FIELDS.SENT_ID) + "," + readerSentences.document(scored_sentences[1].doc, idField_sentence).get(ParsedSentence.FIELDS.SENT_ID);
        else
            result = "";
        return result;
    }

    @Override
    public void search() throws Exception {

        System.out.printf("%n#### Start searching ####%n");

        // the start time of the searching
        final long start = System.currentTimeMillis();

        final Set<String> idField = new HashSet<>();
        idField.add(ParsedDocument.FIELDS.ID);
        idField.add(ParsedDocument.FIELDS.SENTENCES);
        idField.add(ParsedDocument.FIELDS.STANCE);
        idField.add(ParsedDocument.FIELDS.CONCLUSION);
        idField.add(ParsedDocument.FIELDS.SOURCE_ID);

        Query q;
        TopDocs docs;
        ScoreDoc[] sd;
        //String docID;
        String docStance;
        String returnedSentences;

        try {
            for (QualityQuery t : topics) {
                System.out.printf("Searching for topic %s.%n", t.getQueryID());

                BooleanQuery.Builder bq = new BooleanQuery.Builder();
                //expand the query using synonyms coming from WordNet contained in DESCRIPTION or NARRATIVE
                CharArraySet set =
                        new CharArraySet(Arrays.asList((t.getValue(AbstractSearcher.TOPIC_FIELDS.DESCRIPTION) +
                                t.getValue(TOPIC_FIELDS.NARRATIVE)).split("\\s+")), true);
                final Analyzer a = new WordNetQueryExpander(5, set);
                qp = new MultiFieldQueryParser(new String[]{ParsedDocument.FIELDS.TEXT, ParsedDocument.FIELDS.CONCLUSION}, a);
                bq.add(qp.parse(QueryParserBase.escape(t.getValue(TOPIC_FIELDS.TITLE))), BooleanClause.Occur.SHOULD);
                q = bq.build();
                docs = searcher.search(q, maxDocsRetrieved);
                sd = docs.scoreDocs;

                int rank = 1;
                for (ScoreDoc scoreDoc : sd) { //trying to search sentences
                    //docID = reader.document(scoreDoc.doc, idField).get(ParsedDocument.FIELDS.ID);
                    docStance = reader.document(scoreDoc.doc, idField).get(ParsedDocument.FIELDS.STANCE);
                    Document doc = searcher.getIndexReader().document(scoreDoc.doc);
                    returnedSentences = decideSentencesToReturn(doc, t.getValue(TOPIC_FIELDS.TITLE));
                    if (!returnedSentences.equals("")) {
                        //create old files with document instead of sentences
                        //run.printf(Locale.ENGLISH, "%s\t%s\t%s\t%d\t%.6f\t%s%n", t.getQueryID(), docStance, docID, rank++, scoreDoc.score, runID);
                        //create current files with sentences
                        run.printf(Locale.ENGLISH, "%s\t%s\t%s\t%d\t%.6f\t%s%n", t.getQueryID(),
                                docStance.substring(1, docStance.length() - 1), returnedSentences, rank++, scoreDoc.score, runID);
                    }
                }
                run.flush();
            }
        } finally {
            run.close();
            reader.close();
        }
        elapsedTime = System.currentTimeMillis() - start;
        System.out.printf("%d topic(s) searched in %d seconds.", topics.length, elapsedTime / 1000);
        System.out.print("#### Searching complete ####\n");
    }

    /**
     * Main method of the class. Just for testing purposes.
     *
     * @param args command line arguments.
     * @throws Exception if something goes wrong while indexing.
     */
    public static void main(String[] args) throws Exception {

        final String topics = "topics/topics-task-1-1-50.xml";
        final String indexDocsPath = "experiment/index-document-stop-kstem";
        final String indexSentPath = "experiment/index-sentence-stop-kstem";
        final String runPath = "runs";
        final String groupID = "seupd2122-6musk-"; // "D'Artagnan-";
        final String methodID="stop-kstem-wordnet";
        final int maxDocsRetrieved = 1000;
        final String runID = "seupd2022-query-exp";
        WordNetSearcher s = new WordNetSearcher(new BM25Similarity(), indexDocsPath, indexSentPath, topics, 50, runID, runPath, maxDocsRetrieved);
        s.search();
    }
}
