/** This class is used for searching sentences. DirectoryIndexer must store every sentence as a single lucene document. Every time a document is retrieved, it's easy to use the conclusion field to search across all the sentences of one or more of the original documents. */
/*
 *  Copyright 2021-2022 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package src.main.search;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.benchmark.quality.QualityQuery;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParserBase;
import org.apache.lucene.search.*;
import org.apache.lucene.search.similarities.LMDirichletSimilarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;
import src.main.analyze.ToucheAnalyzer;
import src.main.parse.ParsedDocument;
import src.main.parse.ParsedSentence;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;


/**
 * Searches a document collection.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class SentencesSearcher extends AbstractSearcher {
    /**
     * The index reader for the sentences
     */
    private final IndexReader readerSentences;

    /**
     * The index searcher for the sentences
     */
    private final IndexSearcher searcherSentences;

    /**
     * Creates a new searcher.
     *
     * @param similarity       the {@code Similarity} to be used.
     * @param indexDocsPath    the directory where containing the index of documents to be searched.
     * @param indexSentPath    the directory where containing the index of sentences to be searched.
     * @param topicsFile       the file containing the topics to search for.
     * @param expectedTopics   the total number of topics expected to be searched.
     * @param runID            the identifier of the run to be created.
     * @param runPath          the path where to store the run.
     * @param maxDocsRetrieved the maximum number of documents to be retrieved.
     * @throws NullPointerException     if any of the parameters is {@code null}.
     * @throws IllegalArgumentException if any of the parameters assumes invalid values.
     */
    public SentencesSearcher(Similarity similarity, String indexDocsPath, String indexSentPath, String topicsFile, int expectedTopics, String runID, String runPath, int maxDocsRetrieved) {
        super(similarity, indexDocsPath, topicsFile, expectedTopics, runID, runPath, maxDocsRetrieved);

        final Path indexDir = Paths.get(indexSentPath);
        if (!Files.isReadable(indexDir)) {
            throw new IllegalArgumentException(
                    String.format("Index directory %s cannot be read.", indexDir.toAbsolutePath().toString()));
        }

        if (!Files.isDirectory(indexDir)) {
            throw new IllegalArgumentException(String.format("%s expected to be a directory where to search the index.",
                    indexDir.toAbsolutePath().toString()));
        }

        try {
            readerSentences = DirectoryReader.open(FSDirectory.open(indexDir));
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("Unable to create the index reader for directory %s: %s.",
                    indexDir.toAbsolutePath().toString(), e.getMessage()), e);
        }

        searcherSentences = new IndexSearcher(readerSentences);
        searcherSentences.setSimilarity(similarity);
    }

    /**
     * Returns the two sentences that best match the query title.
     *
     * @return a string containing the ids (separated by a comma) of the two sentences that best match the query title.
     */
    @Override
    protected String decideSentencesToReturn(Document doc, String queryTitle) throws ParseException, IOException {
        String result;

        HashMap<String, String> values = new HashMap<>();
        values.put(ParsedDocument.FIELDS.CONCLUSION, doc.get(ParsedDocument.FIELDS.CONCLUSION));
        values.put(ParsedDocument.FIELDS.ID, doc.get(ParsedDocument.FIELDS.ID));

        final Set<String> idField_sentence = new HashSet<>();
        idField_sentence.add(ParsedSentence.FIELDS.ID);
        idField_sentence.add(ParsedSentence.FIELDS.TYPE);
        idField_sentence.add(ParsedSentence.FIELDS.SENT_ID);
        idField_sentence.add(ParsedSentence.FIELDS.SENT_TEXT);

        BooleanQuery.Builder bq_new = new BooleanQuery.Builder();
        QueryParser qp_new = new QueryParser(ParsedSentence.FIELDS.SENT_TEXT, new ToucheAnalyzer());
        bq_new.add(qp_new.parse(QueryParserBase.escape(queryTitle)),BooleanClause.Occur.SHOULD);
        Query idQuery = new WildcardQuery(new Term(ParsedDocument.FIELDS.ID, values.get(ParsedDocument.FIELDS.ID)));
        bq_new.add(idQuery, BooleanClause.Occur.MUST);

        Query q_new = bq_new.build();
        TopDocs sentences_retrieved = searcherSentences.search(q_new, 2);
        ScoreDoc[] scored_sentences = sentences_retrieved.scoreDocs;

        if(scored_sentences.length >= 2)
            result = readerSentences.document(scored_sentences[0].doc, idField_sentence).get(ParsedSentence.FIELDS.SENT_ID) + "," + readerSentences.document(scored_sentences[1].doc, idField_sentence).get(ParsedSentence.FIELDS.SENT_ID);
        else
            result = "";

        return result;
    }


    public void search() throws IOException, ParseException {

        System.out.printf("%n#### Start searching ####%n");

        // the start time of the searching
        final long start = System.currentTimeMillis();

        final Set<String> idField = new HashSet<>();
        idField.add(ParsedDocument.FIELDS.ID);
        idField.add(ParsedDocument.FIELDS.SENTENCES);
        idField.add(ParsedDocument.FIELDS.STANCE);
        idField.add(ParsedDocument.FIELDS.CONCLUSION);
        idField.add(ParsedDocument.FIELDS.SOURCE_ID);

        Query q;
        TopDocs docs;
        ScoreDoc[] sd;
        //String docID;
        String docStance;
        String returnedSentences;

        try {
            for (QualityQuery t : topics) {

                System.out.printf("Searching for topic %s.%n", t.getQueryID());
                BooleanQuery.Builder bq = new BooleanQuery.Builder();

                final Analyzer a = new ToucheAnalyzer();
                qp = new MultiFieldQueryParser(new String[]{ParsedDocument.FIELDS.TEXT, ParsedDocument.FIELDS.CONCLUSION}, a);

                bq.add(qp.parse(QueryParserBase.escape(t.getValue(TOPIC_FIELDS.TITLE))), BooleanClause.Occur.SHOULD);
                q = bq.build();
                docs = searcher.search(q, maxDocsRetrieved);
                sd = docs.scoreDocs;

                int rank = 1; // Using the if inside the for messes with the rank
                for (ScoreDoc scoreDoc : sd) { //trying to search sentences
                    //docID = reader.document(scoreDoc.doc, idField).get(ParsedDocument.FIELDS.ID);
                    docStance = reader.document(scoreDoc.doc, idField).get(ParsedDocument.FIELDS.STANCE);

                    Document doc = searcher.getIndexReader().document(scoreDoc.doc);
                    returnedSentences = decideSentencesToReturn(doc, t.getValue(TOPIC_FIELDS.TITLE));
                    if (!returnedSentences.equals("")) {
                        //create old files with document instead of sentences
                        //run.printf(Locale.ENGLISH, "%s\tQ0\t%s\t%d\t%.6f\t%s%n", t.getQueryID(), docID, rank++, scoreDoc.score, runID);

                        //create current files with sentences
                        run.printf(Locale.ENGLISH, "%s\tQ0\t%s\t%d\t%.6f\t%s%n", t.getQueryID(), returnedSentences, rank++, scoreDoc.score, runID);
                    }
                }
                run.flush();
            }
        } finally {
            run.close();
            reader.close();
        }
        elapsedTime = System.currentTimeMillis() - start;
        System.out.printf("%d topic(s) searched in %d seconds.", topics.length, elapsedTime / 1000);
        System.out.printf("#### Searching complete ####%n");
    }

    /**
     * Main method of the class. Just for testing purposes.
     *
     * @param args command line arguments.
     * @throws Exception if something goes wrong while indexing.
     */
    public static void main(String[] args) throws Exception {

        final String topics = "topics/topics-task-1-1-50.xml";
        final String indexDocsPath = "experiment/index-document-stop-kstem";
        final String indexSentPath = "experiment/index-sentence-stop-kstem";
        final String runPath = "runs";
        final String groupID = "seupd2122-6musk-"; // "D'Artagnan-";
        final String methodID="stop-kstem-3";
        final int maxDocsRetrieved = 1000;
        SentencesSearcher s = new SentencesSearcher(new LMDirichletSimilarity(), indexDocsPath, indexSentPath, topics, 50, groupID+methodID, runPath, maxDocsRetrieved);
        s.search();
    }

    /**
     * Creates all possible word n-grams to be used for searching, using ToucheAnalyzer
     * @param title the string to use for creating all possible word n-grams
     * @return the array of n-grams
     */
    static private String[] getShingleQuery(String title){
        String[] str = new String[1024]; //max value 1024
        int i = 0;
        Analyzer analyzer = new ToucheAnalyzer();
        TokenStream tokenStream = analyzer.tokenStream("SHINGLE", title);

        CharTermAttribute attr = tokenStream.addAttribute(CharTermAttribute.class);
        try {
            tokenStream.reset();
            while (tokenStream.incrementToken() && i < 1024) {
                str[i++] = attr.toString();
            }
            tokenStream.close();
        }
        catch (IOException e){
            System.out.println("error");
        }
        return Arrays.copyOf(str, i);
    }
}
