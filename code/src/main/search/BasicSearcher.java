/*
 *  Copyright 2021-2022 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package src.main.search;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.custom.CustomAnalyzer;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.apache.lucene.benchmark.quality.QualityQuery;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParserBase;
import org.apache.lucene.search.*;
import org.apache.lucene.search.similarities.LMDirichletSimilarity;
import org.apache.lucene.search.similarities.Similarity;
import src.main.analyze.ToucheAnalyzer;
import src.main.parse.ParsedDocument;
import src.main.parse.ParsedSentence;

import java.io.IOException;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * Searches a document collection.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class BasicSearcher extends AbstractSearcher {
    /**
     * Creates a new searcher.
     *
     * @param similarity       the {@code Similarity} to be used.
     * @param indexPath        the directory where containing the index to be searched.
     * @param topicsFile       the file containing the topics to search for.
     * @param expectedTopics   the total number of topics expected to be searched.
     * @param runID            the identifier of the run to be created.
     * @param runPath          the path where to store the run.
     * @param maxDocsRetrieved the maximum number of documents to be retrieved.
     * @throws NullPointerException     if any of the parameters is {@code null}.
     * @throws IllegalArgumentException if any of the parameters assumes invalid values.
     */
    public BasicSearcher(Similarity similarity, String indexPath, String topicsFile, int expectedTopics, String runID, String runPath, int maxDocsRetrieved) {
        super(similarity, indexPath, topicsFile, expectedTopics, runID, runPath, maxDocsRetrieved);
    }

    /**
     * Returns a basic couple of sentences associated with a query output document.
     *
     * @return a basic couple of sentences associated with a query output document.
     */
    @Override
    protected String decideSentencesToReturn(Document doc, String queryTitle) {
        ParsedSentence[] p = SerializationUtils.deserialize(doc.getBinaryValue(ParsedDocument.FIELDS.SENTENCES).bytes);

        int sentC = 0;
        int kconc = -1;
        String sent = "";
        int k = 0;
        //System.out.println(p.length);
        while (k < p.length){
            if(p[k].getSent_id().contains("CONCLUSION")){
                kconc = k;
                sent = sent + p[k].getSent_id() + ",";
                sentC++;
                break;
            }
            k++;
        }
        for(int i = 0; i < p.length; i++){
            if(i != kconc){
                sentC++;
                sent = sent + p[i].getSent_id();
                if(sentC == 2){
                    break;
                }
                else{
                    sent = sent + ",";
                }
            }
        }
        return sent;
    }

    public void search() throws IOException, ParseException {

        System.out.printf("%n#### Start searching ####%n");

        // the start time of the searching
        final long start = System.currentTimeMillis();

        final Set<String> idField = new HashSet<>();
        idField.add(ParsedDocument.FIELDS.ID);
        idField.add(ParsedDocument.FIELDS.SENTENCES);
        idField.add(ParsedDocument.FIELDS.STANCE);

        Query q;
        TopDocs docs;
        ScoreDoc[] sd;
        String docID;
        String docStance;
        String sent;

        try {
            for (QualityQuery t : topics) {

                System.out.printf("Searching for topic %s.%n", t.getQueryID());
                BooleanQuery.Builder bq = new BooleanQuery.Builder();

                qp = new QueryParser(ParsedDocument.FIELDS.CONCLUSION, new ToucheAnalyzer());

                bq.add(qp.parse(QueryParserBase.escape(t.getValue(TOPIC_FIELDS.TITLE))), BooleanClause.Occur.SHOULD);
                q = bq.build();
                docs = searcher.search(q, maxDocsRetrieved);
                sd = docs.scoreDocs;

                int rank = 1;
                for (ScoreDoc scoreDoc : sd) {
                    docStance = reader.document(scoreDoc.doc, idField).get(ParsedDocument.FIELDS.STANCE);


                    Document doc = searcher.getIndexReader().document(scoreDoc.doc);
                    ParsedSentence[] data = SerializationUtils.deserialize(doc.getBinaryValue(ParsedDocument.FIELDS.SENTENCES).bytes);

                    if (data.length >= 2) {
                        //create old files with document instead of sentences
                        docID = reader.document(scoreDoc.doc, idField).get(ParsedDocument.FIELDS.ID);
                        //run.printf(Locale.ENGLISH, "%s\tQ0\t%s\t%d\t%.6f\t%s%n", t.getQueryID(), docID, rank++, scoreDoc.score, runID);

                        //create current files with sentences
                        //this version of decideSentencesToReturn() returns either a conclusion (if it exists) and a premise
                        //or two premises (there is no particular order in which sentences are selected, just the first two)
                        sent = decideSentencesToReturn(doc, "");
                        run.printf(Locale.ENGLISH, "%s\tQ0\t%s\t%d\t%.6f\t%s%n", t.getQueryID(), sent, rank++, scoreDoc.score, runID);
                    }
                }
                run.flush();
            }
        } finally {
            run.close();
            reader.close();
        }
        elapsedTime = System.currentTimeMillis() - start;
        System.out.printf("%d topic(s) searched in %d seconds.", topics.length, elapsedTime / 1000);
        System.out.printf("#### Searching complete ####%n");
    }

    /**
     * Main method of the class. Just for testing purposes.
     *
     * @param args command line arguments.
     * @throws Exception if something goes wrong while indexing.
     */
    public static void main(String[] args) throws Exception {

        final String topics = "topics/topics-task-1-1-50.xml";
        final String indexPath = "experiment/index-document-stop-kstem";
        final String runPath = "runs";
        final String groupID = "seupd2122-6musk-"; // "D'Artagnan-";
        final String methodID="stop-kstem-basic";
        final int maxDocsRetrieved = 1000;
        BasicSearcher s = new BasicSearcher(new LMDirichletSimilarity(), indexPath, topics, 50, groupID+methodID, runPath, maxDocsRetrieved);
        s.search();
    }
}
