package src.main.parse;

import java.io.*;
import java.util.*;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.fasterxml.jackson.core.json.JsonReadFeature;


/**
 * Corpus parser using the Jackson Library
 */

public class ToucheParser extends DocumentParser {

    /**
     * The currently parsed entry/document
     */
    private ParsedDocument document;

    /**
     * Reader for the fields
     */
    Reader targetReader;

    /**
     * The iterator for the entries/documents
     */
    MappingIterator<Map<String, String>> it;

    HashMap<String, String> hashMapDocs = new HashMap<>();
    int doubleLinesDeleted = 0;

    /**
     * Create a new document parser.
     * @param in the reader to the document to be parsed.
     * @throws NullPointerException if {@code in} is {@code null}.
     */
    public ToucheParser(final Reader in) throws IOException {
        super(new BufferedReader(in));
        CsvSchema schema = CsvSchema.builder()
                .addColumn("id")
                .addColumn("conclusion")
                .addColumn("premises")
                .addColumn("context")
                .addColumn("sentences")
                .build()
                .withSkipFirstDataRow(true); //skipping the first row, since contain only the name of the fields
        CsvMapper mapper = new CsvMapper();
        it = mapper
                .readerForMapOf(String.class)
                .with(schema) // !!! IMPORTANT
                .readValues(in);
    }
    @Override
    public boolean hasNext(){
        try{
            //check if it has next entry and get it
            if(!it.hasNextValue()) return false;
            Map<String,String> entry = it.nextValue();

            while(hashMapDocs.containsKey(entry.get("id"))){
                doubleLinesDeleted++;

                if(!it.hasNextValue()) return false;
                entry = it.nextValue();
            }

            hashMapDocs.put(entry.get("id"), entry.get("id"));

            //creating new Parsed document and with the given id
            document = new ParsedDocument(entry.get("id"));
            //setting the conclusion field
            document.setConclusion(entry.get("conclusion"));

            //CONTEXT NOT INDEXED, JUST SOURCE_ID NEEDED
            //creating JsonParser for the context field
            targetReader = new StringReader(entry.get("context"));
            JsonParser contextParser = new JsonFactory().createParser(targetReader);
            contextParser.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES,true);
            contextParser.configure(JsonReadFeature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER.mappedFeature(),true);
            //parsing context field
            final JsonNode contextRoot = new ObjectMapper().readTree(contextParser);

            //filling the details for the context field
            document.setSourceId(contextRoot.get("sourceId").toString());
            /*document.setAcquisitionTime(contextRoot.get("acquisitionTime").toString());
            document.setAspects(contextRoot.get("aspects").toString());
            document.setDiscussionTitle(contextRoot.has("discussionTitle") ? contextRoot.get("discussionTitle").asText():null);
            document.setMode(contextRoot.get("mode").toString());
            document.setSourceDomain(contextRoot.get("sourceDomain").toString());

            document.setSourceText(contextRoot.get("sourceText").toString());
            document.setSourceTextConclusionStart(contextRoot.get("sourceTextConclusionStart").toString());
            document.setSourceTextConclusionEnd(contextRoot.get("sourceTextConclusionEnd").toString());
            document.setSourceTextPremiseStart(contextRoot.get("sourceTextPremiseStart").toString());
            document.setSourceTextPremiseEnd(contextRoot.get("sourceTextPremiseEnd").toString());
            document.setSourceTitle(contextRoot.get("sourceTitle").toString());
            document.setSourceUrl(contextRoot.has("sourceUrl") ? contextRoot.get("sourceUrl").asText():null);*/


            //creating JsonParser for the premises field
            String premises = entry.get("premises");
            premises = premises.substring(1,premises.length()-1);
            targetReader = new StringReader(premises);
            JsonParser premisesParser = new JsonFactory().createParser(targetReader);
            premisesParser.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES,true);
            premisesParser.configure(JsonReadFeature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER.mappedFeature(),true);
            //parsing the premises field
            final JsonNode premisesRoot = new ObjectMapper().readTree(premisesParser);
            document.setText(premisesRoot.get("text").toString());
            document.setStance(premisesRoot.get("stance").toString());
            document.setAnnotations((premisesRoot.get("annotations").toString()));


            //filling the details for the sentences POJO
            String sentences = entry.get("sentences");
            ObjectMapper sentencesMapper = new ObjectMapper();
            sentencesMapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
            sentencesMapper.configure(JsonReadFeature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER.mappedFeature(), true);
            ParsedSentence[] root = sentencesMapper.readValue(sentences, ParsedSentence[].class);


            document.setParsedSentences(root);

            return true;
        }
        catch (IOException e){
            throw new IllegalArgumentException("Read failed : ", e);
        }

    }

    public ParsedDocument parse(){
        return document;
    }

    public static void main(String[] args) throws Exception {
        String PATH = "corpus/args_processed.csv";
        BufferedReader reader = new BufferedReader(new FileReader((PATH)));
        ToucheParser p = new ToucheParser(reader);

        int i =0;
        System.out.println("Start parser and delete double lines");
        while(p.hasNext()){
            if(p.parse().getIdentifier().equals("Se5bee3f3-A53e4b6b6")) System.out.println(p.parse());
            if(p.parse().getIdentifier().equals("Se5bee3f3-Aa48d2efa")) System.out.println(p.parse());
        }

        System.out.println("Number of lines deleted : " + p.doubleLinesDeleted);
        System.out.println("Documents parsed: "+ ++i);
        System.out.println("PARSING DONE!");
    }
}



