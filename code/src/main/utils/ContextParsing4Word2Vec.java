package src.main.utils;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import src.main.analyze.ToucheAnalyzer;
import src.main.analyze.Word2VecAnalyzer;
import src.main.parse.DocumentParser;
import src.main.parse.ParsedDocument;
import src.main.parse.ToucheParser;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Parse the SourceText field of the Context to be then used for vocabulary building
 * because Tokenizer of Word2Vec library not enough powerful
 */
public class ContextParsing4Word2Vec {
    public static void main(String[] args) throws Exception{//GET context from corpus
        Set<String> hashSet= new HashSet<>();
        String originalSourceText;

        DocumentParser dp = DocumentParser.create(ToucheParser.class, Files.newBufferedReader(Paths.get("corpus/args_processed.csv"), Charset.forName("UTF-8")));

        BufferedWriter out = new BufferedWriter(new FileWriter("args_only_context.txt", true));

        Analyzer analyzer = new Word2VecAnalyzer();
        int docsCount =0;

        for (ParsedDocument pd : dp) {
            if (hashSet.contains(pd.getSourceId())) continue;
            hashSet.add(pd.getSourceId());
            docsCount++;
            originalSourceText = pd.getSourceText();

            TokenStream tokenStream = analyzer.tokenStream("FOO", originalSourceText);
            CharTermAttribute attr = tokenStream.addAttribute(CharTermAttribute.class);
            tokenStream.reset();
            while(tokenStream.incrementToken()) {
                out.write(attr.toString()+" ");
            }
            out.write("\n");
            tokenStream.close();

            if (docsCount % 1000 == 0) {
                System.out.printf("%d document(s) indexed.%n", docsCount);
            }
        }
    }
}
