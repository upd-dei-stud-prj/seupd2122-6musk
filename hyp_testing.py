import os
import re

import numpy as np
from scipy import stats as stats
import matplotlib.pyplot as plt
from tabulate import tabulate
import pandas as pd

runs = ["kstem-stop-shingle3", "stop-kstem-basic", "stop-kstem-concsearch", 
        "stop-wordnet-kstem-dirichlet", "word2vec-sentences-kstem"]

results_dir = os.path.relpath("results")
files = []
indexes = []

for i in range(0, len(runs)):
    # change here in case you want to compute statistics for relevance or coherence
    filename = os.path.join(results_dir, "seupd2122-6musk-" + runs[i] + "-quality-res.txt")
    if os.path.isfile(filename):
        indexes.append(i+1)
        files.append(filename)

found_runs = len(files)
ap_all_runs = []
ndcg_all_runs = []

print("Matching files found: ", found_runs)
for f in files:
    ap_values = []  # avg precision
    ndcg_values = []  # cut 5
    for line in open(f):
        res_ap = re.search(r'^map\s+\d+\s+(\d.\d+)', line)  # only on topics
        res_ndcg = re.search(r'^ndcg_cut_5\s+\d+\s+(\d.\d+)', line)
        if res_ap:
            ap_values.append(float(res_ap.group(1)))
        elif res_ndcg:
            ndcg_values.append(float(res_ndcg.group(1)))
    if len(ap_values) != 50:
        raise ValueError("50 topics were expected, got ", len(ap_values), " instead!")
    if len(ndcg_values) != 50:
        raise ValueError("50 topics were expected, got ", len(ndcg_values), " instead!")
    ap_all_runs.append(ap_values)
    ndcg_all_runs.append(ndcg_values)

# print(*ap_all_runs)

#compute ANOVA one way

F_ap, p_ap = stats.f_oneway(*ap_all_runs)
F_ndcg, p_ndcg = stats.f_oneway(*ndcg_all_runs)

print("AP Fstat = ", F_ap, ", AP Pvalue = ", p_ap)
print("NDCG Fstat = ", F_ndcg, ", NDCG Pvalue = ", p_ndcg)

fig, axs = plt.subplots(1, 2, figsize=(15, 4))
# axs[0].set_xlim(0, 8)
axs[0].set_title('AP boxplot')
axs[0].set_xlabel('runs')
box_plots = [axs[0].boxplot(ap_all_runs, showmeans=True, widths=0.4, positions=indexes),
             axs[1].boxplot(ndcg_all_runs, showmeans=True, widths=0.4, positions=indexes)]

axs[1].set_title('NDCG_CUT_5 boxplot')
axs[1].set_xlabel('runs')
fig.subplots_adjust(left=0.1, right=0.9, bottom=0.15, top=0.9, wspace=0.2)

means = [np.mean(ap_all_runs, axis=1), np.mean(ndcg_all_runs, axis=1)]
std = [np.std(ap_all_runs, axis=1), np.std(ndcg_all_runs, axis=1)]
#  print(means)

for i, bp in enumerate(box_plots):
    for j, line in enumerate(bp['medians']):
        x, y = line.get_xydata()[1]
        text = ' μ={:.3f}\n σ={:.3f}'.format(means[i][j], std[i][j])
        axs[i].annotate(text, xy=(x, y), fontsize=7)

plt.show()

#compute t statistic

df_APp = pd.DataFrame(columns=range(0, found_runs), index=range(0, found_runs))
df_NDCGp = pd.DataFrame(columns=range(0, found_runs), index=range(0, found_runs))

for i in range(0, found_runs):
    for j in range(i + 1, found_runs):
        ap_values_1 = ap_all_runs[i]
        ap_values_2 = ap_all_runs[j]
        ndcg_values_1 = ndcg_all_runs[i]
        ndcg_values_2 = ndcg_all_runs[j]
        sa, pa = stats.ttest_ind(ap_values_1, ap_values_2)
        df_APp.loc[i, j] = pa
        sn, pn = stats.ttest_ind(ndcg_values_1, ndcg_values_2)
        df_NDCGp.loc[i, j] = pn

print(tabulate(df_APp, headers=indexes, floatfmt=".4f", tablefmt='psql', numalign="center", showindex=indexes))
print(tabulate(df_NDCGp, headers=indexes, floatfmt=".4f", tablefmt='psql', numalign="center", showindex=indexes))
