\section{Related Work}
\label{sec:related}

\paragraph{On Argument Search and Retrieval}\mbox{}\newline Argumentation is a daily occurrence for taking individual and collaborative decisions. We follow the definition of an argument as proposed by [Walton et al. 2008] to be a conclusion (claim) supported by a set of premises (reason) and to be conveying a stance on a controversial topic [Freeley and Steinberg, 2009]. Sometimes the premise can be left implicit (enthymemes) and the mechanism to draw the conclusion from the premises is informal.


 
The Web is the most important and extensive source of information, and everyone will rely on Google at some point to fill their lack of knowledge. However, as much as search engines usually provide fast and correct answers for factual information, it is not so straightforward when there are multiple controversial opinions \cite{shahshahani2020argument}. Also, fake news worsen their effectiveness, forcing users to check the credibility of sources \cite{samadi2016claimeval} (e.g. the specific website he is visiting). As an example, determining the stance of Twitter users towards messages may constitute an indirect way to identify the truthfulness of discussed rumors \cite {gorrell-etal-2019-semeval}. \newline The corpus we have is preprocessed from the argument search engine args.me\footnote{https://www.args.me/index.html}, which clearly identifies sentences (both premises and conclusions) and the premise’s stance towards conclusion, but crawling the Web to extract such data is a research field on its own. Automatically detecting argumentative content in natural language text, i.e. argument mining, can help to determine people’s opinion on a given topic, why they hold it, and extract insight on public matters, such as politics \cite{huning2022detecting}. More details about argument mining can be found in the survey by \citet{lawrence2020argument}.
 
 
\paragraph{Touch\'e 2021}\mbox{}\newline Our starting point was the Overview of Touch\'e 2021: Argument Retrieval  paper \cite{BondarenkoEtAl2021}, which provided valuable insights on what is argumentation, how we can assess its quality and what retrieval approaches were successful past editions, being this the third one.\newline  
 
 
Winning techniques:
\begin{itemize}
\item DirichletLM similarity function is better at ranking arguments than BM25, DPH, and TF-IDF.
\item Query expansion through Wordnet synonyms/antonyms.
\item  Using relevance judgments of previous years for parameter optimization.
\item Employing transformers to represent documents, such as BERT or SBERT, which are state of the art for argument retrieval \cite{akiki2020exploring} \cite{behrendt2021arguebert}.
\item Quality-based reranking.
\end{itemize} We developed our system taking into account the first two points and using qrels for identifying the best combinations based on our experiments.
 
 

\paragraph{Query Expansion}\mbox{}\newline Query Expansion (QE) is a technique that automatically expands the initial query issued by the user, usually with a human controlled thesaurus. It helps to reduce ambiguity and to increase recall. We added synonyms to the original query in two different ways, namely with WordNet (vocabulary-based) and Word2vec (corpus-based). The interested reader can find more about QE in the comprehensive survey done by \citet{azad2019query}, which provides an overview on core methodologies, use cases and state of the art approaches. Current techniques adopt transformer-based models which show more promising results than classical ones.
 
\paragraph{On Stance Detection and Sentiment Analysis}\mbox{}\newline This year we face an increased layer of complexity: we have to retrieve two sentences with the same stance, and identify if this last one is "pro" or "con" the given topic.
The Webis Group\footnote{https://webis.de/} already started tackling the task of predicting if two arguments share the same stance, though the Same Side Stance Classification Shared task \cite{stein2021same}, which achieved promising results and showed that it’s both feasible and there’s room for improvement.\newline
Retrieving a relevant pair of sentences is closely related both to stance detection, for which a tutorial can be found in \cite{kuccuk2022tutorial}\footnote{https://dkucuk.github.io/stancedetection/}, and to sentiment analysis. For example, \citet{alshari2020senti2vec} used a Word2vec model to expand SentiWordNet, a lexical dictionary for sentiment analysis to learn the polarity of words in the corpus, and evaluated their approach on the IMDB with two classifiers (Logistic Regression and SVM). To estimate the polarity of each non-opinion word in the vocabulary, they computed the score of a given word based on the polarity of the closest term present in SentiWordNet \cite{baccianella2010sentiwordnet}, with encouraging performance in identifying “positive” and “negative” reviews .
Also, current research on stance detection aims at finding the position of a person from a piece of text they produce, focusing on social media portals \cite{aldayel2021stance}.