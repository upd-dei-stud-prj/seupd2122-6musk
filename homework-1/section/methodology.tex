\section{Methodology}
\label{sec:methodology}

%Describe the methodology you have adopted, the architecture of your system, your workflow, etc.
In this section we address how the system was developed starting from the source code of HelloTipster\cite{HelloTipster} repository developed by Professor Nicola Ferro and presented to us during the Search Engine course. Furthermore, we will highlight which parts we focused more on and, more in details, what we tried to improve in the base system.


\subsection{Base system, our first steps into IR}
As previously stated, the system was built in a first iteration by basing our work on the source code provided by Professor Nicola Ferro. In this sense, we spent the first part of the development phase building a basic functioning pipeline to retrieve documents as in last year's edition of Touch\'e Task 1. After concluding the previous step, we turned to this year's challenges and focused on upgrading the base system to retrieve sentences and increasing the performance by trying both off-the-shelf components and custom ones. \newline
The three main components of the basic pipelines are the following:

\subsubsection{ToucheParser}
%Describe how the parsing is done and which fields are used.
Since the corpus was provided as a large .csv file, the first important step was to parse it keeping in mind the memory usage. 
\texttt{ToucheParser} is a class which specializes the abstract \texttt{DocumentParser} written by Professor Nicola Ferro, an Iterable class used to run through the corpus and to index each document using the \texttt{DocumentIndexer}.\newline
In particular, by using a \texttt{BufferedReader} and the Jackson \texttt{CsvMapper}, each line of the corpus was programmatically transformed into a proper fielded instance of \texttt{ParsedDocument} which could then be easily used to index the document with \texttt{Lucene} \cite{ApacheLucene}.\newline
%Describe \textsf{ParsedDocument} and \texttt{ParsedSentence}.
In detail, each \texttt{ParsedDocument} contains:
\begin{itemize}
    \item The ID of the document.
    \item The Premises field of the corpus.
    \item The Stance of the document, which was present as sub-field of the Premises field of the corpus.
    \item The Text sub-field of the Premises field, containing all the sentences of the document as one single string.
    \item The Conclusion of the document, present as a standalone field of the corpus.
    \item The sentences of the document parsed as instances of \texttt{ParsedSentence}. 
\end{itemize}
In turn, each \texttt{ParsedSentence} contains:
\begin{itemize}
    \item The ID of the original document, which we use later to retrieve faster the sentences.
    \item The ID of the sentence, named \texttt{sent\_id}.
    \item The type of sentence, which can be divided in Premise or Conclusion, and which we use later with the ID of the document.
    \item The text of the sentence, which can be found as the sub-field \texttt{sent\_text}.
\end{itemize}

\subsubsection{DirectoryIndexer}
%Describe how the initial document index was created from the ParsedDocument(s) and which fields were present in the index initially (with binary data from sentences, context.text, conclusion, stance, id)
Once we parsed the lines from the corpus file correctly and created an Iterable class (\texttt{ToucheParser}) to obtain a \texttt{ParsedDocument} one by one, we proceeded to index all the documents by using the \texttt{Lucene} API.\newline
For indexing execution, our class expected to get 365408 documents, previously extracted from \texttt{ToucheParser}. 
As already mentioned, the initially implemented \texttt{StandardAnalyzer} belongs to the packet 
\texttt{org.apache.lucene.analysis.Analyzer}. It is a simple Analyzer instantiated using the \texttt{builder()} method, where we specified a \texttt{StandardTokenizerFactory} and a \texttt{LowerCaseFilterFactory} to make all texts lowercase.
In addition, the \texttt{BM25Similarity} similarity function has been used as a starting point to match the documents with the topic. (In the next sections it will be shown how we were able to change the analyzer and the similarity function).

In detail, the fields we had extracted for each document are:
\begin{itemize}
    \item \texttt{ParsedDocument.FIELDS.ID} for the document identifier
    \item \texttt{ParsedDocument.FIELDS.STANCE} for the stance of the document
    \item \texttt{ParsedDocument.FIELDS.CONCLUSION} for the conclusion
    \item \texttt{ParsedDocument.FIELDS.TEXT} for the text field of the document
\end{itemize}

\subsubsection{BasicSearcher}
%Describe how we used StandardAnalyzer and BM25Similarity to match documents to queryTitle 
After checking the integrity of the index using Luke(Lucene Index Toolbox) and verifying all the fields were containing the expected content, we created a basic searcher to retrieve documents starting from source code developed by Professor Ferro.\newline
In particular, after opening an \texttt{IndexSearcher} and using \texttt{ToucheTopicsReader} to read each topic, the searcher constructs a query from the title of the topic only. The searcher matches it with the text and the conclusion of the documents in the index providing the best matching documents for each topic (at most 1000).
For the initial searcher, \texttt{StandardAnalyzer} and \texttt{BM25Similarity} were used as Analyzer and similarity function to be coherent with what we used for indexing.\newline
As it will be shown in the results section, after this part we were able to run the system using last year's topics and relevance judgments to check the base performance for our system.  


\subsection{Different approaches for Sentence Selection}
%small introduction to the subsection
The second planned iteration of the development required upgrading the searcher(s) to select pairs of sentences in a meaningful way, instead of single documents. \newline
For this phase of the development, we decided to upgrade \texttt{BasicSearcher} firstly to retrieve the conclusion and the first premise of the initially selected documents trivially, or, if no conclusion was present, add another premise to the output. This sparked the idea for two different approaches with a common idea at the core, doing sentence selection from the document initially matched with the last year's version of the system. In our view, this is beneficial since pairs of sentences from the same document have intuitively higher coherence than arbitrary pairs of sentences from the entire collection. \newline

The two different searchers developed from the previous intuition are the following:

\subsubsection{SentencesSearcher}
%How we used AbstractDirectoryIndexer for DRY, how we chose the Analyzer for each index.
\texttt{SentencesSearcher} is the first of the two new solutions. The development of the class required the generalization of \texttt{BasicSearcher} into an abstract class \texttt{AbstractSearcher} to follow the DRY principle and avoid repetitions. \texttt{AbstractSearcher} was used to implement the shared portion of code and later became the superclass for all the rest of the searchers. \newline
%Describe how we created the second index for the sentences and why 2 indexes. 
Until this point, the sentences were not correctly indexed to provide fast matching between them and the topics, so it became necessary to index them. To separate the document retrieval from the sentences retrieval we chose to create a new index. As in the case of the searchers we made use of an abstract class called \texttt{AbstractDirectoryIndexer} that contained the shared code (DRY principle) for \texttt{DirectoryIndexerDocument}, the indexer for the documents, and \texttt{DirectoryIndexerSentences}, the index for the sentences. \newline
%Describe how the sentences are selected in details.
To sum up, the searcher works by retrieving for each topic the required number of documents using the index constructed by \texttt{DirectoryIndexerDocument} and then, for each document, providing the two sentences from that document that best match the topic title using the index constructed by \texttt{DirectoryIndexerSentences}.

\subsubsection{ConclusionSearcher}
%Describe the difference with SentencesSearcher and why we think this is a better approach to retrieve coherent pair of sentences
After developing \texttt{SentencesSearcher}, we decided to push more on the idea of coherence between sentences of the same document. \texttt{ConclusionSearcher} works in a similar way to \texttt{SentencesSearcher}. For each topic, it retrieves the required number of documents using the index constructed by \texttt{DirectoryIndexerDocument} and then, chooses the 2 sentences by selecting always the first conclusion of the document and the premise (of the same document) which better matches the text of the conclusion. To implement this second step, we constructed a query that finds only the relevant premises in the index created by \texttt{DirectoryIndexerSentences} and matches them with the text of the conclusion.


\subsection{Increasing performance}
Once the full system for this year edition of Task 1 was completed and different sentence selection methods were explored, we decided to focus on improving the performance of the overall system.
The way in which we decided to divide the system was beneficial in experimenting with ideas since we were able to evaluate them using last year relevance judgment. We've tried query expansions because it was listed among the winning approaches of last year and wanted to see its effectiveness against the new topics.
Evaluation was fundamental in selecting the runs we would then submit to the organizers. \newline
Following there are the details of the various off-the-shelf components tried and the custom query expansion techniques used to improve performance.

\subsubsection{Trying out off-the-shelf components}
%Describe the components tried, how we added the ToucheAnalyzer.
To try different off-the-shelf components it was necessary to develop our own analyzer. The class \texttt{ToucheAnalyzer}, which is based on the work of Professor Nicola Ferro, contains all the code used to test these components. \newline
The main components explored were:
\begin{itemize}
\item Stoplists and Stopword Removal. In this case the tries done are limited to two different stoplists seen during the course: '\textit{glasgow.txt}' and '\textit{smart.txt}'.
\item Stemming. The stemmers tried were \texttt{PorterStemFiler} and \texttt{KStemFilter}, versions of the Porter stemmer and the Krovetz stemmer found in the \texttt{Lucene} library .
\item Character N-grams and Word N-grams (Shingles). Also in this case the filters were implemented by \texttt{Lucene}, named respectively \texttt{NGramTokenFilter} and \texttt{ShingleFilter}. 
\end{itemize}
The components were chosen using an heuristic approach. Starting from the baseline model we added one component at a time and selected at each iteration the best performing between the ones available.
%\paragraph{Shingle run description}
%To create both the index for the original documents and the sentences, and for analyzing the query we’ve applied the Krovetz stemmer, two stoplists (\textit{glasgow.it} %and \textit{smart.txt}), and shingles up to 3 words. We’ve changed the default filler token of \texttt{ShingleFilter(tokens, 3)} for the query, to avoid too many not %relevant matches (e.g., “in the” after stop word removal becomes “\_ \_”, so could potentially match “not in”). \texttt{SentencesSearcher} was used in both phases, to %retrieve documents and sequences, with the topic title as the query. 

% descrivere query expansion (2 parti? daniel e marco?)
\subsubsection{Query expansion based on WordNet database}
WordNet is a popular database which is described at \url{https://wordnet.princeton.edu/}. Different versions can be found online: for convenience reasons which we will explain in a moment, we adopted a Prolog version of WordNet 3.1, which can be found at \url{https://github.com/ekaf/wordnet-prolog}. WordNet is a network where words are linked to other similar words which not only may share a stem, but which may also be semantically related; thus it groups words based on their meanings so words in close proximity to one another in the network are semantically equivalent.

There is a package provided by \texttt{Lucene} which enables us to add a synonym filter from a WordNet-like database in Prolog. It loads the database in a \texttt{SynonymMap} which is a fast hash map used to retrieve synonyms from any specified lowercase word. After creating this map, we can add a \texttt{SynonymTokenFilter} to our analyzer, passing the former to the constructor. We explicitly made an analyzer called  \texttt{WordNetQueryExpander} which does other things among setting up a synonym filter. In this order, the first step consists in setting up a lower case filter, then a stop filter, and finally the synonym filter. The constructor of the analyzer accepts a parameter \texttt{set} which enables us to keep only synonyms words contained in \texttt{set}: this has been used to speed up the process of searching as we will describe shortly. The second step applies this keep only filter if \texttt{set} is not null, and then applies the Krovetz Stemmer.


So the new searcher which is implemented in \texttt{WordNetSearcher} is an extension of \texttt{Conclu\-sionSearcher} in the following sense: the query (topic title) is expanded using all synonyms from WordNet and searched among all documents (using Dirichlet similarity function); for each document among the top 1000 retrieved by the index searcher, the conclusion is again expanded using at most 2 synonyms for each term and searched among all sentences of this document to find the best possible matching premise (since the other selected sentence is necessarily the conclusion). The value 2 is an heuristic number and can be modified as we please: it is hard to define a precise value for the maximum number of synonyms without qrels.

To speed up the process of searching one can avoid to expand the full conclusion with all terms in WordNet, but instead provide a keep only filter (this explains the use of \texttt{set} above): for instance one can decide to keep only synonyms appearing in the topic description, or in the topic narrative (or both) which likely may contain synonyms of words in the topic title. In the end we opted to fully expand the conclusion, even though the process of searching took almost an hour. See \ref{sec:conclusion} for details on speeding things up or alternative designs. 

One final note: we performed the search of the sentences as in \texttt{ConclusionSearcher} since we are confident that it is more likely that conclusion will be expanded further than the query title, as in general it is longer (in fact a mixed approach could be tried as well). We tried 2 combinations of filters for this analyzer (the synonym filter is always applied clearly):\begin{itemize}
\item a combination of lowercase filter, stop filter (Glasgow list), synonym filter, Krovetz stemmer (the default)
\item a combination of lowercase filter and synonym filter
\end{itemize}
both were tried using BM-25 similarity and Dirichlet similarity (see \ref{sec:results} for a discussion of the final results).



\subsubsection{Word2vec-based query expansion}

Word2vec\cite{https://doi.org/10.48550/arxiv.1301.3781} is a natural language technique to produce word embeddings, ideally from a large corpus of text. Each word is represented as a vector of numbers and the cosine similarity computes the semantic relationship between words. It’s a set of neural network models with one hidden layer trained to predict the features of the surrounding window of a given word (Skip-gram) or the target word from a context window (CBOW, continuous-bag-of-words).

CBOW better captures syntactic relationships between words, meanwhile Skip-gram the semantic one. For example, given the term “day” CBOW may retrieve “days”, whereas Skip-gram may retrieve “night” instead, which is semantically close but not syntactically. 
Skip-gram is less sensitive to high frequency words and is less likely to overfit because it looks at single words each iteration, whereas CBOW trains on a window of words, meaning that it sees frequent words more often. 
For the previous reasons, we opted for Skip-gram.


The training data was built removing all duplicates, i.e. documents with the same "sourceId" sub-field, resulting in 58962 documents.
We filtered from "args\_processed\_04\_01.csv" the "sourceText" sub-field, replacing with a whitespace all words with three or more equal consecutive characters, words that contained numbers and the substring "xa0". All terms have been turned lowercase and only ones between 3 and 14 characters were kept.


An initial try was made using the whole context field, but there was too much noise and training was unsuccessful, due to the size of the corpus (longer time) and often retrieved synonyms of a given word  didn't share the same semantic meaning. For performance tuning we looked at the conclusion field of retrieved documents (since we always returned it) to make it relevant w.r.t query.


The model was built using the \href{https://github.com/eclipse/deeplearning4j}{deeplearning4j} library with the following :\newline\newline
\texttt{Word2Vec vec = new Word2Vec.Builder().stopWords(list).minWordFrequency(20)}\newline  \texttt{.layerSize(512).windowSize(10).iterate(iter).tokenizerFactory(t).build();}\newline\newline

\begin{itemize}
  \item \texttt{stopWords(list)} is a stopword list based on '\textit{smart.txt}' plus approximately 50 of the more common terms from the context field.
  \item \texttt{minWordFrequency(20)} forces the removal from the Word2Vec vocabulary of the 20 most commonly used words in the "SourceText" subfield.
  \item \texttt{tokenizerFactory(new StandardTokenizer())} is the default tokenizer of the deeplearning4j library.
  \item \texttt{windowSize} is the number of words used to compute the numeric vector of a given word. Since we already preprocessed "SourceText" and removed many stop words we kept a relatively low value (10). 
  \item \texttt{layerSize} is the number of features in the word vector. Default values are around 100, we used 512 to try to improve the quality, with no apparent downsides. Printing out synonyms for query terms we saw a closer semantic meaning with higher values of this parameter (512 vs 256).

\end{itemize}






\paragraph{Word2Vec run description}

In the first phase, query expansion was performed on the topic title, using two stoplists ('\textit{glasgow.txt}' and '\textit{smart.txt}'). No stemming was performed at this stage because, as can be seen from row 1 and rows 2 \& 3 (student-students) in table \ref{tab:word2vec-synonyms}, we are kind of performing lemmatisation.
Also antonyms may be added (row 6)).
During searching time, only synonyms with a similarity higher than a heuristically found value (0.53 in range 0-1) were added, and at most 5 per term of the original query. 


For the second phase, we’ve used the conclusion field from the retrieved document as input for another query to find the most similar premise in the document (as in \texttt{ConclusionSearcher}). The Krovetz stemmer has been applied and no stop list. 


\begin{table}[pos=H]
  \caption{Example of synonyms/antonyms generated with Word2vec}
  \label{tab:word2vec-synonyms}
  \centering
\begin{tabular}{|l|l|l|l|}
\hline
  & Query Term & Synonym   & Similarity \\ \hline
1 & teachers   & teacher   & 0.76       \\ \hline
2 & teachers   & student   & 0.66       \\ \hline
3 & teachers   & students  & 0.66       \\ \hline
4 & teachers   & classroom & 0.65       \\ \hline
5 & teachers   & schools   & 0.61       \\ \hline
6 & legal      & illegal   & 0.60       \\ \hline
\end{tabular}
\end{table}






%\begin{figure}[pos=H]
%  \centering
%  \includegraphics[width=1\linewidth]{figure/Parse.png}
%  \caption{Parser UML package.}
%  \label{fig:sample-figure}
%\end{figure}

%\begin{figure}[pos=H]
%  \centering
%  \includegraphics[width=1\linewidth]{figure/Analyze.png}
%  \caption{Analyze UML package}
%  \label{fig:sample-figure}
%\end{figure}

%\begin{figure}[pos=H]
%  \centering
%  \includegraphics[width=1\linewidth]{figure/Index.png}
%  \caption{Index UML package.}
%  \label{fig:sample-figure}
%\end{figure}