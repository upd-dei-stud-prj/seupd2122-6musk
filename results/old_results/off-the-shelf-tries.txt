nostop-nostem-relevance 			ndcg_cut_5            	all	0.6457
									map                   	all	0.3828
nostop-nostem-quality 				ndcg_cut_5            	all	0.6691
									map                   	all	0.3051

nostop-nostem-relevance-dirichlet 	ndcg_cut_5            	all	0.6906 <-----
									map                   	all	0.4679 <-----
nostop-nostem-quality-dirichlet		ndcg_cut_5            	all	0.7841 <-----
									map                   	all	0.4125 <-----

We conclude it's best to use LMDirichletSimilarity instead of BM25Similarity.


stop-nostem-relevance-dirichlet 	ndcg_cut_5            	all	0.6800
									map                   	all	0.4532
stop-nostem-quality-dirichlet		ndcg_cut_5            	all	0.7873 <-----
									map                   	all	0.4052

smartstop-nostem-relevance-dirichlet ndcg_cut_5            	all	0.6665	
									map                   	all	0.4457
smartstop-nostem-quality-dirichlet	ndcg_cut_5            	all	0.7818
									map                   	all	0.3994

We conclude it's best to use glasgow.txt as stoplist.


stop-kstem-relevance 				ndcg_cut_5            	all	0.7007 <-----
									map                   	all	0.4542 
stop-kstem-quality					ndcg_cut_5            	all	0.7984 <-----
									map                   	all	0.3987

stop-portstem-relevance 			ndcg_cut_5            	all	0.6765
									map                   	all	0.4435
stop-portstem-quality				ndcg_cut_5            	all	0.7947
									map                   	all	0.3896

We conclude it's best to use kstemmer

nostop-nostem-3gram-relevance		ndcg_cut_5            	all	0.4799
                                    map                   	all	0.2182
nostop-nostem-3gram-quality			ndcg_cut_5            	all	0.5079
                                    map                   	all	0.1777

stop-kstem-shingle3-relevance      ndcg_cut_5            	all	0.6865
									map                   	all	0.4420
stop-kstem-shingle3-quality 		ndcg_cut_5            	all	0.7471
									map                   	all	0.3644

Neither word 3-grams nor character 3-grams improve performance, but we decided to submit a run with word 3-grams and SentenceSearcher.